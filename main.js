/*
This is a simple api to return the current location of the ISS. 
 returns the current latitude and longitude of the space station
 th a unix timestamp for the time the location was valid. 
 This API takes no inputs.

url = http://api.open-notify.org/iss-now.json;

{
  "message": "success", 
  "timestamp": UNIX_TIME_STAMP, 
  "iss_position": {
    "latitude": CURRENT_LATITUDE, 
    "longitude": CURRENT_LONGITUDE
  }
}
*/
async function getLocation() {
  let urlLocation = 'http://api.open-notify.org/iss-now.json';

  let currentLocation = await fetch(urlLocation);

  let location = await currentLocation.json();

  console.log(location);
}
getLocation();

/* function initMap() {
  const myLatLng = { lat: -25.363, lng: 131.044 };
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng,
  });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!",
  });
} */

